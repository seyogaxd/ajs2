const books = [
    {
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70
    },
    {
      author: "Сюзанна Кларк",
      name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    },
    {
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    },
    {
      author: "Террі Пратчетт",
      name: "Рухомі картинки",
      price: 40
    },
    {
      author: "Анґус Гайленд",
    }
  ];
  
  const ul = document.createElement("ul");
  
  for (const book of books) {
    try {
      const hasAllRequiredProperties = book.author && book.name && book.price;
      if (hasAllRequiredProperties) {
        const li = document.createElement("li");
        li.textContent = `${book.author} - ${book.name} - ${book.price}`;
        ul.appendChild(li); 
      }
    } catch (error) {
      continue;
    }
  }
  const rootDiv = document.getElementById('root');
  rootDiv.appendChild(ul);
  